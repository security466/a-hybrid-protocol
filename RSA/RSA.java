/*
 *  @author: Hercy (Yunhao Zhang)
 *  @author: Yi Shi
 */
package RSA;

import gnu.getopt.Getopt;
import java.util.Random;
import java.util.Date;
import java.math.BigInteger;

public class RSA
{
	private static boolean DEBUG_FLAG = false;
	private static boolean DEBUG_primeGenerator = true;
	private static int PUBLIC_KEY_MAX_LENGTH = 60;
	private static int PUBLIC_KEY_MIN_LENGTH = 20;
    private static int CERTAINTY = 200;
	private static boolean kFlag = false;
			
	public static void main(String[] args)
    {
		
		StringBuilder bitSizeStr = new StringBuilder();
		StringBuilder nStr = new StringBuilder();
		StringBuilder dStr = new StringBuilder();
		StringBuilder eStr = new StringBuilder();
		StringBuilder m = new StringBuilder();		
		
		pcl(args, bitSizeStr, nStr, dStr, eStr,m);
		
		if(!bitSizeStr.toString().equalsIgnoreCase(""))
        {
			//This means you want to create a new key
			genRSAkey(bitSizeStr);
		}
		else if(kFlag)
		{
            System.out.println("Bit size not set, bit size will be set to defualt value: 1024.");
			bitSizeStr.append("1024");
			genRSAkey(bitSizeStr);	
		}
		
		if(!eStr.toString().equalsIgnoreCase(""))
        {
			RSAencrypt(m, nStr, eStr);
		}
		
		if(!dStr.toString().equalsIgnoreCase(""))
        {
			RSAdecrypt(m, nStr, dStr);
		}
		
	}


    /** ========================================
     *  RSAencrypt
     *  ----------------------------------------
     *  Encryption
     *  ======================================== */
	private static void RSAencrypt(StringBuilder m, StringBuilder nStr, StringBuilder eStr)
    {
		// TODO Auto-generated method stub
        if(DEBUG_FLAG) /*** DEBUG ***/
        {
            System.out.println("*** @RSAencrypt: Encrypt called!!!\n");
            System.out.println("*** @RSAencrypt: Input public key: " + eStr.toString() + "\n");
            System.out.println("*** @RSAencrypt: Input modulus: " + nStr.toString() + "\n");
            System.out.println("*** @RSAencrypt: Input message: " + m.toString() + "\n");
        }
        
        
        // Initial values from input
        BigInteger e = new BigInteger(eStr.toString(), 16);
        BigInteger n = new BigInteger(nStr.toString(), 16);
        BigInteger M = new BigInteger(m.toString(), 16);
        
        if(DEBUG_FLAG) /*** DEBUG ***/
        {
            System.out.println("*** @RSAdecrypt: e = " + e.toString() + "\n");
            System.out.println("*** @RSAdecrypt: n = " + n.toString() + "\n");
            System.out.println("*** @RSAdecrypt: M = " + M.toString() + "\n");
        }
        
        // Compute C = M^e mod n
        BigInteger C = powerHelper(M, e, n);//(M.pow((int)e.longValue())).mod(n); //TODO FIX THIS
        
        if(DEBUG_FLAG) /*** DEBUG ***/
        {
            System.out.println("*** @RSAdecrypt: C = " + C.toString() + "\n");
            System.out.println("\n========================================");
            System.out.println("*** @RSAencrypt: Result:");
            System.out.println("========================================");

        }
        
        System.out.println("Here's your encrypted chipertext: ");
        System.out.println(C.toString(16));
		
	}


    /** ========================================
     *  RSAdecrypt
     *  ----------------------------------------
     *  Decryption
     *  ======================================== */
	private static void RSAdecrypt(StringBuilder cStr, StringBuilder nStr, StringBuilder dStr)
    {
		// TODO Auto-generated method stub
        if(DEBUG_FLAG) /*** DEBUG ***/
        {
            System.out.println("*** @RSAdecrypt: Decrypt called!!!\n");
            System.out.println("*** @RSAdecrypt: Input private key: " + dStr.toString() + "\n");
            System.out.println("*** @RSAdecrypt: Input modulus: " + nStr.toString() + "\n");
            System.out.println("*** @RSAdecrypt: Input ciphertext: " + cStr.toString() + "\n");
        }
        
        BigInteger d = new BigInteger(dStr.toString(), 16);
        BigInteger n = new BigInteger(nStr.toString(), 16);
        BigInteger c = new BigInteger(cStr.toString(), 16);
            
        if(DEBUG_FLAG) /*** DEBUG ***/
        {
            System.out.println("*** @RSAdecrypt: d = " + d.toString() + "\n");
            System.out.println("*** @RSAdecrypt: n = " + n.toString() + "\n");
            System.out.println("*** @RSAdecrypt: c = " + c.toString() + "\n");
        }
            
        // Compute M = c^d mod n
        BigInteger M = powerHelper(c, d, n);//(c.pow((int)d.longValue())).mod(n); //TODO FIX THIS
        
        if(DEBUG_FLAG) /*** DEBUG ***/
        {
            System.out.println("*** @RSAdecrypt: M = " + M.toString() + "\n");
            System.out.println("\n========================================");
            System.out.println("*** @RSAdecrypt: Result:");
            System.out.println("========================================");
            
        }
            
        System.out.println("Here's your decrypted message: ");
        System.out.println(M.toString(16));


	}
	
    /** ========================================
     *  powerHelper
     *  ----------------------------------------
     *  Returns of X^Y mod Z
     *  ======================================== */
    private static BigInteger powerHelper(BigInteger X, BigInteger Y, BigInteger Z)
    {
        BigInteger output = new BigInteger("1");
        BigInteger tmp = X.mod(Z);
        
        while(Y.compareTo(new BigInteger("0")) == 1)
        {
            if(Y.mod(new BigInteger("2")).compareTo(new BigInteger("1")) == 0)
            {
                output = output.multiply(tmp).mod(Z);
            }
            Y = Y.divide(new BigInteger("2"));
            tmp = tmp.multiply(tmp).mod(Z);
        }
        return output;
    }
    
    /** ========================================
     *  genRSAkey(StringBuilder)
     *  ----------------------------------------
     *  Generate and print out the key.
     *  ======================================== */
	private static void genRSAkey(StringBuilder bitSizeStr)
    {
		// TODO Auto-generated method stub
        // Check bit size
		if(DEBUG_FLAG) /*** DEBUG ***/
		{
			System.out.println("*** @genRSAkey: Current bitSizeStr set as: " + bitSizeStr.toString() + "\n");
		}
		int bitSize = Integer.parseInt(bitSizeStr.toString());
        if(bitSize < 1024)
        {
            System.out.println("Current bit size " + bitSize + " is too smaller, will be automaticly set to 1024.");
            bitSize = 1024;
        }
		
        // Generate p
        if(DEBUG_FLAG) /*** DEBUG ***/
        {
            System.out.println("*** @genRSAkey: Generating large prime p.\n");
        }
        
        BigInteger p = primeGenerator(bitSize/2); //new BigInteger(bitSize / 2, CERTAINTY, new Random( (new Date()).getTime() ));
        if(DEBUG_FLAG) /*** DEBUG ***/
        {
            System.out.println("*** @genRSAkey: p = " + p.toString());
        }
        
        // Generate q
        if(DEBUG_FLAG) /*** DEBUG ***/
        {
            System.out.println("*** @genRSAkey: Generating large prime q.\n");
        }
        BigInteger q;
        do
        {
            q = primeGenerator(bitSize/2); //new BigInteger(bitSize / 2, CERTAINTY, new Random( (new Date()).getTime() ));
            if(DEBUG_FLAG) /*** DEBUG ***/
            {
                System.out.println("*** @genRSAkey: q = " + q.toString());
            }
        }
        while(p.equals(q));
        
        
        // Compute n = pq
        BigInteger n = p.multiply(q);
        if(DEBUG_FLAG) /*** DEBUG ***/
        {
            System.out.println("*** @genRSAkey: n = " + n + "\n");
        }
        
        // Compute Phi(n) = (p - 1)(q - 1)
        BigInteger ps1 = p.subtract(new BigInteger("1"));
        BigInteger qs1 = q.subtract(new BigInteger("1"));
        BigInteger phi_n = ps1.multiply(qs1);
        if(DEBUG_FLAG) /*** DEBUG ***/
        {
            System.out.println("*** @genRSAkey: Phi(n) = " + phi_n + "\n");
        }
        
        // Find e, small odd relative prime with Phi(n)
        int size_of_e;
        do
        {
            size_of_e = new Random((new Date()).getTime()).nextInt() % PUBLIC_KEY_MAX_LENGTH;
        }
        while(size_of_e < PUBLIC_KEY_MIN_LENGTH);
        
        BigInteger e = new BigInteger(size_of_e , CERTAINTY, new Random((new Date()).getTime()));
        
        while( (!e.gcd(phi_n).toString().equals("1")) && (e.mod(new BigInteger("2"))).equals(new BigInteger("0")) )
        {
            e = e.nextProbablePrime();
            if(DEBUG_FLAG) /*** DEBUG ***/
            {
                System.out.println("*** @genRSAkey: e = " + e + "\n");
            }

        }
        
        // Compute d = e^-1 mod Phi(n)
        BigInteger d = e.modInverse(phi_n);
        if(DEBUG_FLAG) /*** DEBUG ***/
        {
            System.out.println("*** @genRSAkey: d = " + d + "\n");
        }
        
        
        // Output keys to user:
        if(DEBUG_FLAG) /*** DEBUG ***/
        {
            System.out.println("\n========================================");
            System.out.println("*** @genRSAkey: Result:");
            System.out.println("========================================");
        }
        System.out.println("Your e for public key:");
        System.out.println(e.toString(16));
        //System.out.println(e.toString());
        System.out.println();
        System.out.println("Your d for private key:");
        System.out.println(d.toString(16));
        //System.out.println(d.toString());
        System.out.println();
        System.out.println("Your modulus n: ");
        System.out.println(n.toString(16));
        //System.out.println(n.toString());
	}


    /** ========================================
     *  pcl
     *  ----------------------------------------
     *  This function Processes the Command Line
     *  Arguments.
     *  ======================================== */
	private static void pcl(String[] args, StringBuilder bitSizeStr, StringBuilder nStr, StringBuilder dStr, StringBuilder eStr, StringBuilder m)
    {
		/*
		 * http://www.urbanophile.com/arenn/hacking/getopt/gnu.getopt.Getopt.html
		*/	
		Getopt g = new Getopt("Chat Program", args, "hke:d:b:n:i:");
		int c;
		String arg;
		while((c = g.getopt()) != -1)
        {
            switch(c)
            {
                case 'i':
                    arg = g.getOptarg();
                    m.append(arg);
                    break;
                    
                case 'e':
                    arg = g.getOptarg();
                    eStr.append(arg);
                    break;
                    
                case 'n':
                    arg = g.getOptarg();
                    nStr.append(arg);
                    break;
                    
                case 'd':
                    arg = g.getOptarg();
                    dStr.append(arg);
                    break;
                    
                case 'k':
	                kFlag = true;
	                //bitSizeStr.append("1024");
	                //genRSAkey("1024");
                    break;
                    
                case 'b':
                    arg = g.getOptarg();
                    bitSizeStr.append(arg);
                    break;
                    
                case 'h':
                    callUsage(0);
                case '?':
                    break; // getopt() already printed an error
                    
                default:
                    break;
            }
        }
	}
	
	/** ========================================
     *  callUsage(int)
     *  ----------------------------------------
     *  input: exit status code
     *  Will print usage and exit program with
     *  given exit status.
     *  ======================================== */
    private static void callUsage(int exitStatus)
    {

        String usage =  "    -h: help and usage\n" +
                        "    -k -b <bit_size>: generate a pair of public & private key with the given bit size in hex\n" +
                        "    -e <public_key>  -n <modulus> -i <plaintext_value>:  encrypt\n" +
                        "    -d <private_key> -n <modulus> -i <ciphertext_value>: decrypt";
		
		System.err.println(usage);
		System.exit(exitStatus);
		
	}

    /** ========================================
     *  primeGenerator(int)
     *  ----------------------------------------
     *  input:  the required bit size
     *  return: a prime number with given length
     *  ======================================== */
    private static BigInteger primeGenerator(int bitSize)
    {
        BigInteger prime = new BigInteger("4");
        
        int trial = 1;
        while(!prime.isProbablePrime(CERTAINTY)) /* (!isPrime(prime.longValue())) */
        {
            if(DEBUG_FLAG && DEBUG_primeGenerator) /*** DEBUG ***/
            {
                System.out.println("*** @primeGenerator: Generating...." + "\n");
            }
            
            prime = BigInteger.probablePrime(bitSize, new Random((new Date()).getTime()));
            if(DEBUG_FLAG && DEBUG_primeGenerator) /*** DEBUG ***/
            {
                System.out.println("*** @primeGenerator: Trial " + trial++);
            	System.out.println(prime + "\n");
            }
        }
        if(DEBUG_FLAG && DEBUG_primeGenerator) /*** DEBUG ***/
        {
	        System.out.println("*** @primeGenerator: Final prime is:" + prime + "\n");
	    }
        return prime;
    }
    
    /* isPrime: a helper method checks whether it is prime */
    private static boolean isPrime(long num)
    {
        for(long i = 2; i < num; i++)
        {
            if(i != num && num % i == 0)
            {
                return false;
            }
        }
        return true;
    }

}
