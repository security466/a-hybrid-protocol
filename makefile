#!/bin/bash
all: compile mkalias

#For compile:
compile:
	javac -cp ".:./getopt.jar" CHAT.java

#Make alias:
mkalias:
	alias CHATROOM="java \-cp \".:./getopt.jar\" CHAT"

clean:
	rm -f *.class